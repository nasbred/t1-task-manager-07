package ru.t1.kharitonova.tm.model;

import ru.t1.kharitonova.tm.constant.ArgumentConst;
import ru.t1.kharitonova.tm.constant.CommandConst;

public class Command {

    public static final Command ABOUT = new Command(CommandConst.ABOUT,ArgumentConst.ABOUT, "Display developer info.");
    public static final Command VERSION = new Command(CommandConst.VERSION,ArgumentConst.VERSION, "Display program version.");
    public static final Command HELP = new Command(CommandConst.HELP,ArgumentConst.HELP, "Display list of terminal commands.");
    public static final Command EXIT = new Command(CommandConst.EXIT, null, "Close application.");
    public static final Command INFO = new Command(CommandConst.INFO,ArgumentConst.INFO, "Display system information.");
    private String name;
    private String argument;
    private String description;

    public Command() {
    }

    public Command(String name, String argument, String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String displayName = "";
        if (name != null && !name.isEmpty()) displayName += name;
        if (argument != null && !argument.isEmpty()) displayName += ", " + argument;
        if (description != null && !description.isEmpty()) displayName += ": " + description;
        return displayName;
    }
}
