package ru.t1.kharitonova.tm.constant;

public final class CommandConst {

    public static final String ABOUT = "about";
    public static final String VERSION = "version";
    public static final String HELP = "help";
    public static final String EXIT = "exit";
    public static final String INFO = "info";

}
